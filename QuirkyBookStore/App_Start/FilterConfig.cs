﻿using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace QuirkyBookStore
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
