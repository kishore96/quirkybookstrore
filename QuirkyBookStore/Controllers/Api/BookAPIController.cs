﻿using QuirkyBookStore.Models;
using QuirkyBookStore.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuirkyBookStore.Controllers.Api
{
    public class BookAPIController : ApiController
    {
        private ApplicationDbContext db;
        public BookAPIController()
        {
            db = new ApplicationDbContext();
        }

        //to retrieve Title of the Book
        public IHttpActionResult Get(string query = null)
        {
            var bookQuery = db.Books.Where(b => b.Title.ToLower().Contains(query.ToLower()));

            return Ok(bookQuery.ToList());
        }

        //Price or Availability(Price / Avail)
        public IHttpActionResult Get(string type, string isbn = null, string rentalDuration = null, string email = null )
        {
            if (type.Equals("price"))
            {
                Book bookQuery = db.Books.Where(b => b.ISBN.Equals(isbn)).FirstOrDefault();

                var chargeRate = from u in db.Users
                                 join m in db.MembershipTypes on u.MembershipTypeId equals m.Id
                                 where u.Email.Equals(email)
                                 select new { m.ChargeRateOneMonth, m.ChargeRateSixMonth };

                var price = Convert.ToDouble(bookQuery.Price) * Convert.ToDouble(chargeRate.ToList()[0].ChargeRateOneMonth) / 100;

                if (rentalDuration == SD.SixMonthCount)
                {
                     price = Convert.ToDouble(bookQuery.Price) * Convert.ToDouble(chargeRate.ToList()[0].ChargeRateSixMonth) / 100;
                }
                return Ok(price);
            }
            else
            {
                Book bookQuery = db.Books.Where(b => b.ISBN.Equals(isbn)).FirstOrDefault();
                return Ok(bookQuery.Availability);
            }          
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}
