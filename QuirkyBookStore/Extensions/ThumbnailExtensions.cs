﻿using QuirkyBookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls.Expressions;

namespace QuirkyBookStore.Extensions
{
    public static class ThumbnailExtensions
    {
        public static IEnumerable<ThumbnailModel> GetBookThumbnail(this List<ThumbnailModel> thumbnailModels, ApplicationDbContext db = null, string search = null)
        {
            try
            {
                if (db == null) db = ApplicationDbContext.Create();

                thumbnailModels = (from b in db.Books
                                   select new ThumbnailModel
                                   {
                                       BookId = b.Id,
                                       Title = b.Title,
                                       Description = b.Description,
                                       ImageUrl = b.ImageUrl,
                                       Link = "/BookDetail/Index/" + b.Id,
                                   }).ToList();

                if (search != null)
                {
                    return thumbnailModels.Where(t => t.Title.ToLower().Contains(search.ToLower())).OrderBy(t => t.Title);
                }
            }
            catch (Exception ex)
            {

            }
            return thumbnailModels.OrderBy(b => b.Title);

        }
    }
}