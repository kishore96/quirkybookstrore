﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuirkyBookStore.Startup))]
namespace QuirkyBookStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
